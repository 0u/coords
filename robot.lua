-- mocks for the robot api in opencomputers.

local r = {}

-- called functions will be stored here, for use in tests.
-- for example running this code
-- r.turnLeft()
-- r.forward()
-- calls would be {'turnLeft', 'forward'}
r.calls = {}

local function true_or_false(b)
  -- allow forcing the return of a function (like forward)
  if b ~= nil then
    return b
  end

  if math.random(0, 1) == 1
    then return true
    else return false
  end
end

r.up      = true_or_false
r.down    = true_or_false
r.forward = true_or_false
r.back    = true_or_false

r.turnLeft  = function() end
r.turnRight = function() end

r.detect     = true_or_false
r.detectDown = true_or_false
r.detectUp   = true_or_false

r.swing     = true_or_false
r.swingUp   = true_or_false
r.swingDown = true_or_false

-- add the recording middleware.
for name, fn in pairs(r) do
  if type(fn) == 'function' then
    r[name] = function(...)
      table.insert(r.calls, name)
      return fn(...)
    end
  end
end

return r
