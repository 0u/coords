local r = require 'coords'

local test = {}
local printf =  function(s, ...)    return print(s:format(...)) end
local assertf = function(c, s, ...) return assert(c, s:format(...)) end

local resetCoords = function()
  r.c.x = 0
  r.c.y = 0
  r.c.z = 0
  r.c.ori = 0
end

-- check that r.c is equal to want (excluding orientation)
-- and not checking for extra keys.
local coordsEq = function(want)
  assert(r.c.x == want.x, ('want %d, got %d'):format(want.x, r.c.x))
  assert(r.c.y == want.y, ('want %d, got %d'):format(want.y, r.c.y))
  assert(r.c.z == want.z, ('want %d, got %d'):format(want.z, r.c.z))
end

----------------------------------- TESTS -----------------------------------

local turnTest = function(turnFn, inc)
  return function()
    for i=1,5 do
      local ori = r.c.ori
      local want = (ori + inc) % 4

      turnFn()
      assert(r.c.ori == want, ('want %d, got %d'):format(want, r.c.ori))
    end
  end
end

test.turnRight = turnTest(r.turnRight, 1)
test.turnLeft  = turnTest(r.turnLeft, -1)

function test.moveTo()
  -- test moving to some coordanites
  local tests = {
    {x = 10,  y = 20,  z = 30, ori = 2},
    {x = -10, y = -30, z = 20, ori = 3},
    {x = 30,  y = 0,   z = 3,  ori = 0},
  }

  for _, want in ipairs(tests) do
    resetCoords()
    r.moveTo(want)
    coordsEq(want)
    assert(want.ori == r.c.ori, ('want ori %d, got ori %d'):format(want.ori, r.c.ori))
  end

  -- make sure that moveTo deals with bad arguments by erroring.
  local args = {
    {x = 10, y = nil, z = 5, ori = 0},
    {},
    {x = 'a', y = 'b', z = 'c', ori = 'd'},
  }

  for i, arg in ipairs(args) do
    local ok, err = pcall(function() r.moveTo(arg) end)
    assertf(not ok, 'expected error, got none (test #%d)', i)
  end
end

function test.look()
  -- if you do r.calls = * then it will not set the calls field of the __index table.
  getmetatable(r).__index.calls = {}
  r.c.ori = 0

  -- Test turning right
  for i=1,3 do
    r.look(i)
    assertf(r.calls[i] == 'turnRight', 'want turnRight, got %s', r.calls[i])
  end

  -- Test turning left
  getmetatable(r).__index.calls = {}
  r.c.ori = 3
  for i=3,0 do
    r.look(i)
    assertf(r.calls[i] == 'turnLeft', 'want turnLeft, got %s', r.calls[i])
  end

  -- Test turning around
  getmetatable(r).__index.calls = {}
  r.c.ori = 0

  r.look('south')
  -- we don't care what direction we turn, we just want to turn around.
  assertf(r.calls[1]:match('turn.*'), 'want turn*, got %s', r.calls[1])
  assertf(r.calls[2]:match('turn.*'), 'want turn*, got %s', r.calls[2])
end

----------------------------------- TESTS -----------------------------------

local seed = os.time()
printf('Testing with random seed: %d', seed)
math.randomseed(seed)

local pad = 25
for name, tc in pairs(test) do
  io.write('TEST ' .. name .. (' '):rep(pad-name:len()) .. '| ')
  local ok, err = pcall(tc)
  print(err or 'OK')
end
